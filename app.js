const express = require('express');
const app = express()
const path = require('path')
const exphbs = require('express-handlebars')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const upload = require('express-fileupload')
const {mongodbURL} = require('./config/database')



//======== file uploading ==================
app.use(upload()) // this line always declare before body parser

//body parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//============= database setup ==========================
mongoose.connect(mongodbURL, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
    .then(db => {
        console.log('database connected');
    }).catch(err=>console.log(`error is ===== ${err}`))


app.use(express.static(path.join(__dirname, 'public')))

//======== Setup View Engine  =============
app.engine('handlebars', exphbs({ defaultLayout: 'home'}))
app.set('view engine', 'handlebars')

//====== Load roiutes ===============
const home = require('./routes/home/index')


//============= Use routes ============
app.use('/', home);

//============= local host connections ================
let port = 3000 || process.env.port
app.listen(port, () => console.log(`Server started on port ${port}`))