const mongoose = require('mongoose')
const Schema = mongoose.Schema

const informationSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    phone:{
        type: String 
    },
    image:{
        type: String 
    },
    file:{
        type: String
    }
})

module.exports = mongoose.model('information',informationSchema)